class CfgPatches
{
    class armaforces_sma_cba_mag_compat
    {
        name = "ArmaForces - SMA CBA Mag Compat";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"SMA_Weapons", "cba_main"};
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
